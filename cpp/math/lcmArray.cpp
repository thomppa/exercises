#include <bits/stdc++.h>
#include <iostream>

using namespace std;

typedef long long int ll;

int gcd(int a, int b)
{
 if(b==0)
     return a;
 return gcd(b, a % b);
}

int lcm(int arr[], int n)
{
    ll ans = arr[0];

    for (int i = 0; i < n ; i++) 
    {
    ans = (((arr[i] * ans)) / (gcd(arr[i], ans)));
    }
    return ans;
}

int main()
{
    int arr[] = {23,2,5,3};

    int n = sizeof(arr) / sizeof(arr[0]);

    cout << "LCM of array is: " << 
        lcm(arr, n) << endl;
    return 0;
}
