#include <algorithm>
#include <bits/stdc++.h>
#include <iostream>

using namespace std;

static int gcd(int a, int b)
{
 if(a==0)
     return b;
 return gcd(b % a, a);
}

static int lcm(int a, int b)
{
    return (a*b) /gcd(a,b);
}

int main()
{
    int a = 25;
    int b = 110;

    cout << "Least Common Multiplier is: "<< lcm(a , b) << endl;
}
