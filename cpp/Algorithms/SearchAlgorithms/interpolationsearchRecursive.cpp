#include <bits/stdc++.h>
#include <iostream>

using namespace std;

int interpolationrec(int arr[], int l, int r, int x)
{
    int pos;

    if (l <= r && x >= arr[l] && x <= arr[r]) {
        pos = l + (((double)(r - l) / (arr[r] - arr[l]) * (x - arr[l])));

        if (arr[pos] == x)
            return pos;

        if (arr[pos] < x)
            return interpolationrec(arr, pos + 1, r, x);
        if (arr[pos] > x)
            return interpolationrec(arr, l, pos - 1, x);
    }
    return -1;
}

int main(void)
{
    int arr[] = { 1, 2, 3, 4, 5, 12, 14, 15, 17, 99 };
    int x = 17;
    int n = sizeof(arr) / sizeof(arr[0]);
    int result = interpolationrec(arr, 0, n - 1, x);
    (result == -1) ? cout << "Element is not present!" : cout << "Element is present at index " << result;

    return 0;
}
