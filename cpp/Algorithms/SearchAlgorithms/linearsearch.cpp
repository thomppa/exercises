#include <iostream>
#include <ostream>

using namespace std;

//linear serch is pretty straight forward
//The search starts from the leftmost element of arr[]
//if x matches an Element, the index is returned
//if x does not match any of the elements return -1
//Time-Space Complexity = O(n)

int search(int arr[], int n, int x)
{
    int i;
    for (i = 0; i < n; i++)
    {
        if (arr[i] == x) {
        return i;
        }
    }
    return -1;
}


int main(int argc, char const *argv[]) {
    int arr[] = {2,3,4,10,40,9,12,13,56,32,12};
    int x = 13;
    int n = sizeof(arr) /sizeof(arr[0]);
    int result = search(arr, n, x);
    (result == -1)? cout << "Element is not present" : cout << "Element is present at index " << result << endl;

  return 0;
}
