#include <bits/stdc++.h>

using namespace  std;


//Binarysearch searches an array repeatedly by dividing the search interval in half
//if the value of the search key is less the Item in the middle, narrow down the interval to the lower half
//otherwise narrow it to the upper half
//repeat until value is found

//Time Complexity O(log n)

int Binarysearch(int arr[], int l, int r, int x)
{
  if (r >= l) 
    {
    int mid = l + (r -l) / 2;

    if (arr[mid] == x)
    {
        return mid;
    }
    if (arr[mid] > x)
    {
        return Binarysearch(arr, l, mid -1, x);
    }
    return Binarysearch(arr, mid +1, r , x);
    }
    return -1;
}
int main(void)
{
    int arr[] = {1,2,3,4,5};
    
    int x = 12;
    int n = sizeof(arr) / sizeof(arr[0]); //Gesamtspeicher des Arrays durch Speicher eines Indexes = Länge des Arrays
    int result = Binarysearch(arr, 0, n - 1, x);
    (result == -1) ? cout << "Element is not present!" : cout << "Element is present at index " << result << endl;
  return 0;
}
