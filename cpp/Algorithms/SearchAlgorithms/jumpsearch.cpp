#include <algorithm>
#include <cmath>
#include <iostream>
#include <ostream>
#include <math.h>
#include <bits/stdc++.h>

using namespace std;

//Die Time Complexity liegt zwichen 0(n) und O(Log n)
//Die Jumpsearchmethode ist damit langsamer als Binarysearch, hat aber den Vorteil,
//das nur einmal der Weg zurück eingeschlagen werden muss

int JumpSearch(int arr[], int n, int x)
{

    //bestimmen der Blockgröße 
    //Optimale Blockgröße ist Wurzel aus n
    int m = sqrt(n);

    int prevM = 0;

    //Hier wird nach dem Block gesucht, in dem sich x befindet
    //wenn er sich überhaupt im array befindet.

    while (arr[min(m,n)-1] < x) 
    {
            prevM = m; //Immer die letzte Instanz von m vor der aktuellen

            m += sqrt(n); //neue Instanz von m solange, bis m größer ist als x

            if (prevM >= n) //Wenn dieser Fall eintritt ist das Element nicht im Array
            {
                return -1;
            }
    }
    
    while (arr[prevM] < x) 
    {
        prevM++;
        //wenn das Ende des Blocks erreichts ist
        //oder das Ende des Arrays, ist das Element nicht präsent
        if (prevM == min(m,n))
        {
        return -1;
        }
    }
    //wenn das Element gefunden wurde
    if (arr[prevM] == x) {
        return prevM;
    }
    return -1;
}

int main(void)
{
    int arr[] = {1,1,2,3,4,5,8,13,21,34,55,89,144,233,377,610};
    int n = sizeof(arr)/sizeof(arr[0]);
    int x = 377;
    int result = JumpSearch(arr,n,x);
    (result == -1)? cout << "Element is not present" : cout << "Element is present at index " << result << endl;
        return 0;
}
