#include <algorithm>
#include <bits/stdc++.h>
#include <iostream>

using namespace std;

int search(int arr[], int l, int r, int x)
{
    if(r < l)
        return -1;
    if(arr[l] == x)
        return l;
    if(arr[r] == x)
        return r;
    return search(arr, l +1 , r -1, x);
}

int main()
{
    int arr[] = {34,12,25,16,54,19,30};
    int r = sizeof(arr)/sizeof(arr[0]);
    int l = 0;
    int x = 12;
    int index = search(arr, l, r -1,  x);

    if (index!=-1)
    {
    cout << "The index is : " << index << endl;
    }
    else {
    cout << "The element is not present" << endl;
    }

    return 0;

}
