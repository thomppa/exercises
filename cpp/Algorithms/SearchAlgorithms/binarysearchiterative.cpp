#include <bits/stdc++.h>
#include <iostream>
#include <vector>

using namespace std;

int binarysearch(int arr[], int l, int r, int x) {
  while (l <= r) {
    cout << l << endl;
    int m = l + (r - l) / 2;

    // Check if x is present at mid
    if (arr[m] == x) {
      return m;
    }
    // If x greater, ignore left half
    if (arr[m] < x) {
      ` l = m + 1;
    }
    // If x smaller, ignore right half
    else {
      r = m - 1;
    }
  }
  // If here, Element not present
  return -1;
}

int main(void) {
  int arr[] = {1, 2, 3, 4, 5, 12, 14, 15, 17, 99};
  int x = 17;
  int n = sizeof(arr) / sizeof(arr[0]);
  int result = binarysearch(arr, 0, n - 1, x);
  (result == -1) ? cout << "Element is not present!"
                 : cout << "Element is present at index " << result;
}
