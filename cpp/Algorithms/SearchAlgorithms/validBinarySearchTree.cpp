#include <bits/stdc++.h>
#include <deque>
#include <iostream>

using namespace std;

class Node {
public:
    Node(int v)
        : val(v)
        , left(nullptr)
        , right(nullptr) {};
    int val;
    Node* left;
    Node* right;
};
