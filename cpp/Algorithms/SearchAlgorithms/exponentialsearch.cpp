#include <algorithm>
#include <bits/stdc++.h>
#include <cstdio>

using namespace std;

//Time Complexity: O(log n)
//this implementation is recursive and requires O(log n) space.
//with iterative Binary Search, we need only O(1) space

int binarySearch(int arr[], int, int, int);

int exponentialSearch(int arr[], int n, int x)
{
    //if x is present at first location itself
    if(arr[0] == x)
        return 0;

    //The idea is to start with a subarray of size 1, compare it's last element with x then try size 2, then 4, 8 etc, so until the last Element of a subaray is not greater.
    //Once there is an index greater than x, we know, that the value is somehere between i/2 and i. Becaus i/2 is the last known instance of i from the last iteration.
    int i = 1;
    
    //Find the range for binary Searching through repeated doubling
    while (i < n && arr[i] <= x)
    {
    i = i*2; 
    }
    //Call binary search for the found range
    return binarySearch(arr,i/2, min(i,n), x); 
}

int binarySearch(int arr[], int l, int r, int x)
{
    while (l<=r)
    {
        int mid = l + (r - l)/2;
        
        if (arr[mid] == x) 
        {
        return mid;
        }

        if (arr[mid] > x) 
        {
        return binarySearch(arr,l, mid -1, x);
        }
        else {
        return binarySearch(arr, mid+1, r, x);
        }
    }

    return -1;
}

int main(void)
{
    int arr[] = {1,2,3,4,5,12,14,15,17,99};
    int x = 99;
    int n = sizeof(arr) / sizeof(arr[0]);
    int result = exponentialSearch(arr, n, x);
    (result == -1) ?  printf("Element is not present!") : printf("Element is present at index %d.", result);
}
