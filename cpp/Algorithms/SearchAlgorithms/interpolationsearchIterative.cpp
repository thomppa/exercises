#include <iostream>
#include <math.h>
#include <bits/stdc++.h>

using namespace std;

//pos = lo + [x-arr[lo]] * (hi -low) /arr[hi]-arr[lo])]
//arr[] ==> Array which is searched
//x ==> Element to be searched
//lo ==> starting index in arr[]
//hi ==> Ending index in arr[] == n-1

//If all Elements in the Array are uniformly distributed then 0(log log n)
//in worst case it's O(n)
//Auxiliary Space: O(1)

int InterpolationSearch(int arr[],int n,int x)
{
    int l = 0;
    int r = n-1;

    while(l <=r && x >= arr[l] && x <= arr[r]) 
    {
        if(l == r)
        {
            if (arr[l] == x)
            {
                return l;
            }
            else
            {
                return -1;
            }
        }
        int pos = l + (((double)(r - l) /(arr[r] -arr[l]))*(x - arr[l]));
    
        if (arr[pos] == x) 
        {
        return pos; 
        }
        if (arr[pos] < x)
        {
        l = pos +1;
        }
        else 
        {
        r = pos -1;
        }
    }
    return -1;
}

int main(void)
{
    int arr[] = {1,2,3,4,5,12,14,15,17,99};
    int x = 17;
    int n = sizeof(arr) / sizeof(arr[0]);
    int result = InterpolationSearch(arr, n, x);
    (result == -1) ? cout << "Element is not present!" : cout << "Element is present at index " << result;

return 0;
}

