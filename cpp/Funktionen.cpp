#include <iostream>

#include "string"

using namespace std;

void printmulti(string word, int n)
{
  for (int i = 0; i < n; i++)
  {
    std::cout << word << '\n';
  }
}

int main(int argc, char const *argv[]) {
  printmulti("Hallo Welt!",4);
  return 0;
}
