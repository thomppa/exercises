#include <cstddef>
#include <iostream>
#include <iterator>

using namespace std;

struct Node
{
    int data;
    Node* next;
};

void printData(Node* n)
{
    while(n!=0)
    {
    cout << n->data << " ";
    n = n->next;
    }
}

int main()
{
    //create 3 Pointers for Node
    Node*   head = NULL;
    Node*   second = NULL;
    Node*   third = NULL;

    //allocate 3 nodes in the heap
    head = new Node();
    second = new Node();
    third = new Node();

    head ->data = 1; //assign data in first Node
    head->next = second;

    second ->data = 2;//assign data in second Node
    second->next = third;

    third->data = 3;//assign data in third Node
    third->next = NULL;

    printData(head);

    return 0;
}

