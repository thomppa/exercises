#include <bits/stdc++.h>

using namespace std;

void showStack(stack <int> stk)
{
    while (!stk.empty())
    {
        cout << '\t' << stk.top();
        stk.pop();
    }

    cout << '\n';
}

int main()
{
    stack <int> s;
    s.push(10);
    s.push(20);
    s.push(30);
    s.push(40);
    s.push(50);

    cout << "The stack is : ";
    showStack(s);

    cout << "\ns.size() : " << s.size();
    cout << "\ns.top() : " << s.top();

    cout << "\ns.pop() : ";
    s.pop();
    showStack(s);

    cout << "\ns.top() : " << s.top();
}
