#include <algorithm>
#include <bits/stdc++.h>
#include <iostream>

using namespace std;

int search(int arr[], int l, int r, int x)
{
    if(l > r)
        return -1;

    int mid = (l+r) /2;

    if(arr[mid] == x)
        return mid;

    if(arr[l] <= arr[mid])
    {
        if(x >= arr[l] && x <= arr[mid])
            {
            return search(arr , l, (mid -1), x);
            }
        return search(arr, mid +1, r, x);
    }

    if(x >= arr[mid] && x < arr[r])
        return search(arr, mid + 1, r, x);

    return search(arr,l, mid, x);
}

int main()
{
    int arr[] = {7,8,9,10,11,12,1,2,3,4,5,6};
    int n = sizeof(arr) / sizeof(arr[0]);

    int x = 3;
    int i = search(arr, 0, n-1, x);

    if (i != -1)
    {
    cout << "Index is : " << i << endl;
    }
    else {
    cout << "Index not found!" << endl;
    }
}
