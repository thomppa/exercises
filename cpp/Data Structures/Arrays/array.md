# Using Arrays in Programming

An array is a collection of items stored at contigous memory locations.

The idea is to easier calculate the position of each element by adding an offset to a base value (like the memory location of the first element of the array)

![array](https://media.geeksforgeeks.org/wp-content/uploads/array-2.png)  
  
- Memory Location
- Stored Value
- Index of the Value



