#include <bits/stdc++.h>
#include <iostream>
#include <ostream>

using namespace std;

void printArray(int arr[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << arr[i] << " ";
    }
    cout << endl;
}

void swap(int arr[], int fi, int si, int d)
{
    int temp, i;
    for (i = 0; i < d ; i++) 
    {
        temp = arr[fi +i];
        arr[fi + i] = arr[si +i];
        arr[si +i] = temp;
    }
}
void leftRotate(int arr[], int d, int n)
{
    if (d == 0 || d == n)
    {
    return; 
    }

    if (n - d == d)
    {
    swap(arr, 0, n - d, d);
    return;
    }

    if (d < n - d)
    {
    swap(arr, 0, n-d,d);
    leftRotate(arr, d, n - d);
    //printArray(arr, n);
    }
    else {
    swap(arr, 0,d,n-d);
    leftRotate(arr + n -d, (2*d) -n, d);
    }
}


int main(void)
    {
    int arr[] = {1,2,3,4,5,6,7};
    int n = sizeof(arr) / sizeof(arr[0]);
    leftRotate(arr, 2, n);
    printArray(arr, n);
    }
