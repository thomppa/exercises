#include <bits/stdc++.h>
#include <iostream>

using namespace std;

int Binarysearch(int arr[], int r, int l, int x)
{
    
    if(r < l)
        return -1;

    int mid = (r+l) /2;

    if(x == arr[mid])
        return mid;

    if(x > arr[mid])
        return Binarysearch(arr, (mid + 1), r, x);

    cout << Binarysearch(arr, l, (mid -1), x) << endl;
    return Binarysearch(arr, l, (mid -1), x);

}

int findPivot(int arr[], int l, int r)
{

if(r < l) return -1;

if(r==l) return l;

int mid = (l+r) /2;

if(mid < r && arr[mid] > arr[mid +1])
    return mid;
if(mid > l && arr[mid] < arr[mid -1])
    return arr[mid -1];
if(arr[l] >= arr[mid])
    return findPivot(arr, l, mid -1);

cout << findPivot(arr, mid +1, r);
return findPivot(arr, mid +1,  r);
}

int PivotBinarySearch(int arr[], int n, int x)
{
    int pivot = findPivot(arr,  0,  n -1);

    if (pivot == -1)
    {
        return Binarysearch(arr, 0, n-1, x);
    }

    if(arr[pivot] == x)
        return pivot;
    
    if(arr[0] <= x)
        return Binarysearch(arr, 0, pivot -1, x);
        
    cout << Binarysearch (arr, pivot +1, n-1, x) << endl;
    return Binarysearch(arr, pivot +1, n -1, x);
}


int main()
{
    int arr[] = {4,5,6,1,2,3};

    int n = sizeof(arr) / sizeof(arr[0]);
    
    int x = 3;

    cout << "Index of searched element is: " << PivotBinarySearch(arr, n, x);
    return 0;
}
