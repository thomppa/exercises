#include <bits/stdc++.h>
#include <sstream>

using namespace std;

void swap(int arr[], int fi, int si, int d)
{
    int temp, i;
    for (i = 0; i < d ; i++) 
    {
        temp = arr[fi +i];
        arr[fi + i] = arr[si +i];
        arr[si +i] = temp;
    }
}
void leftRotate(int arr[], int d, int n)
{
    int i,j;

    if (d == 0 || d == n) {
    return;
    }

    i = d;
    j = n -d;

    while (i != j) {
    
        if(i < j)
        {
            swap(arr, d-i, d+j-i, i);
            j -= i;
        }
        else {
            swap(arr, d-i, d, j);
            i -= j;
        }
    }
    swap(arr, d-i, d, i);
}

void printArray(int arr[], int size)
{
    for (int i = 0; i < size; i++)
    {
        cout << arr[i] << " ";
    }
}

int main()
    {
    int arr[] = {1,2,3,4,5,6,7};
    int n = sizeof(arr) / sizeof(arr[0]);
    leftRotate(arr, 3, n);
    printArray(arr, n);
    return 0;
    }
