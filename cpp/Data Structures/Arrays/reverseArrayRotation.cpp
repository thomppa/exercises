#include <bits/stdc++.h>

using namespace std;

//O(n)

void reverseRotate(int arr[], int start, int end)
{
    while (start < end) 
    {
    int temp = arr[start]; //Der Wert des startindex wird als temporäre variable gespeichert.

    arr[start] = arr[end]; //Der Wert des startIndex wird der Wert des Endindex zugewiesen.. 
    arr[end] = temp; //Der Endindex bekommt den Wert der temporären Variable
    start++; //nächster Index als Start
    end--; //Endindex ein Index vor
    }

}

void leftRotate(int arr[], int d, int n)
{
    if (d == 0)
    {
    return; 
    }
    //Im Ursprung ist das Array in 2 Teile aufzuteilen A und B
    reverseRotate(arr, 0, d-1); // = Ar rotieren des ersten Teils des Arrays.
    reverseRotate(arr, d, n-1); // = Br rotieren des 2. Teils
    reverseRotate(arr, 0, n-1); // = BA rotieren der 2 bereits rotierten Teile zusammen 
}

void printArray(int arr[], int size)
{
    for (int i = 0; i < size ; i++ )
    {
    cout << arr[i] << " ";
    }
}

int main()
{
    int arr[] = {1,2,3,4,5,6,7};
    int n = sizeof(arr) / sizeof(arr[0]);
    int d = 3;

    d = d % n;

    leftRotate(arr,d, n);
    printArray(arr, n);

    return 0;
}
