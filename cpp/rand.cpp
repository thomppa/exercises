#include <cmath>
#include <cstdlib>
#include <iostream>
#include <math.h>
#include <random>

using namespace std;

double pi;

double estimate_pi(double n)
{

    double num_point_circle = 0;
    double num_point_total = 0;

    const float min = 0.0;

    const float max = 1.0;

    random_device rand_dev;
    mt19937 generator(rand_dev());
    uniform_real_distribution<double> distrib(min, max);

    //Added a different comment back in
    for (int i = 0; i < n; i++) {

        float x = distrib(generator);
        float y = distrib(generator);

        //std::cout << x <<"x" << '\n';
        //std::cout << y <<"y" << '\n';
        double distance = pow(x, 2) + pow(y, 2);

        num_point_total += 1;

        if (distance <= max) {
            num_point_circle += 1;
        }
    }
    pi = 4 * num_point_circle / num_point_total;

    return pi;
}

int main(int argc, char const* argv[])
{
    estimate_pi(100000000);
    cout << pi << endl;
    return 0;
}
