#include <iostream>
#include <spqr.hpp>

using namespace std;

int main() {

    long _long = pow(2, (sizeof(long) * 8));
    long _int = pow(2, (sizeof(int) * 8));
    long _short = pow(2, (sizeof(short) * 8));

    short i = 4;
    cout << "Der Speicher ,der von einem Short belegt werden kann beträgt:" << endl;
    cout << sizeof(short) << " Bytes" << endl; //zeigt den Speicher in Byte an, den die Variable belegt.
    cout << "Bei 2^16 sind das "<< _short <<" Werte, die speicherbar sind" << endl;
    cout << "Das bedeutet eine Zahlenrange im long zwischen "<<_short - (_short *1.5)<<" ==> "<< _short / 2 << endl;
    cout << endl;
    cout << "Der Speicher, der von einem int belegt werden kann beträgt:" << endl;
    cout << sizeof(int) << " Bytes" << endl; //zeigt den Speicher in Byte an, den die Variable belegt.
    cout << "Bei 2^32 sind das "<< _int <<" Werte, die speicherbar sind" << endl;
    cout << "Das bedeutet eine Zahlenrange im long zwischen "<<_int - (_int *1.5)<<" ==> "<< _int / 2 << endl;
    cout << endl;
    cout << "Der Speicher, der von einem long belegt werden kann beträgt:" << endl;
    cout << sizeof(long) << " Bytes" << endl; //zeigt den Speicher in Byte an, den die Variable belegt.
    cout << "Bei 2^64 sind das "<< _long <<" Werte, die speicherbar sind" << endl;
    cout << "Das bedeutet eine Zahlenrange im long zwischen "<<_long - (_long *1.5)<<" ==> "<< _long / 2 << endl;
    cout << endl;

    double d = 2.5;

    cout << d << endl;

    //unsigned Datentypen decken nur den positiven Zahlenbereich ab
    return 0;
}
