#include <iostream>
#include "string"

using namespace std;

int main() {

    string name ="Erik Mueller";

    //Länge des Strings ermitteln
    cout << name.length() << endl;
    cout << name.size() << endl;

    //Auf ein einzelnes zeichen zugreifen
    cout << name[0] << endl; //gibt einen Char zurück!

    if(name[5] == 'M')
        cout << "An Position 5 steht ein M!" << endl;

    //auf einen Teilstring zugreifen
    cout << name.substr(5,7) << endl; //gehe zur Position 5 im Zeichenarray und gib ab dort 7 Zeichen aus
    cout << name.substr(0,4) << endl; //gehe zur Position 0 im Zeichenarray und gib ab dort 4 Zeichen aus
    //ein Zeichen suchen
    cout << name.find(' ') << endl; //gibt die Position des Zeichens ein

    std::cout << "Hello" << endl;

    cout << name.find('x') << endl;

    cout << string::npos << endl; //wenn kein Zeichen gefunden wird wird diese Konstante wiedergegeben
    cout << string::npos << endl;
    if(name.find('X') == string::npos)
        cout << "Im String taucht x nicht auf" << endl;
    return 0;
}
