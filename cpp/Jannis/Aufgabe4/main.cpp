#include <iostream>
#include <string>

using namespace std;

int main() {
    //Variante 1
    for(int year = 2010; year <= 2022; year++)
        if(year % 4 == 0) continue;
        else cout << year << endl;

        //Variante 2;
    /*for(int year ; year < 2022; year++)
        if(year != 2012 && year != 2016 && year != 2020)
            cout << year << endl;
    */

    return 0;

}
