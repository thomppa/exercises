#include <iostream>

using namespace std;

int main()
{
    string name = "Erik Mueller";
    cout << name << endl;

    name[0] = 'X';

    cout << name << endl;

    name.append("!!!");
    //name = name + "!!!"; //==append
    //name += "!!!" //==append
    cout << name << endl;

    //Zeichen in die Mitte eines Strings packen
    //Achtung, auch sehr langsam!
    name.insert(4, ",");
    cout << name << endl;

    string name2 = "Erik Mueller";

    //Zeichen aus einem String entfernen
    //Hier wird von der 5. Position im Array, 3 darauf folgende
    //Zeichen entfernt.
    name2.erase(5,3);

    cout << name2 << endl;

    return 0;
}
