#include <iostream>

using namespace std;

int main() {

    string name = "Erik Mueller";

    cout << name[5] << endl;
    cout << name.at(5) << endl;
   
    cout << name[5] << endl;


    /*
     *Läuft weiter, auch wenn der Index Überschritten wird, gefährlich, wenn
     *man das für eine Variable nutzen möchte
     */


    /*
     *cout << name.at(12)<< endl;
     *wirft einen fehler aus, wenn der index überschritten wird.
     *gesünder für den code, aber langsamer in der ausführung,
     *allerdings nur minimal.
     *Es ist besser, das Programm mit einem Error zu beenden
     *andernfalls ist das Verhalten des Programs undefined.
     */
     
    for (int i = 0; i < name.length(); i++)
    {
    cout << name[i] << endl;
    }
    return 0;

}
