#include <iostream>
using namespace std;

int main() {
    int a = 5;
    int b = 2;

    cout << (float) 5 / 2 << endl;
    cout << 5.0 / 2 << endl;
    cout << (float)a / b << endl;
    cout << static_cast<double >(a)  / static_cast<double>(b) << endl;

    return 0;
}
