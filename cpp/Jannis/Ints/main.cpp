#include <iostream>

using namespace std;

int main() {

    string greeting = "1234";  //Für den Pc ist das hier eine Zeichenkette
    cout << greeting << endl;

    long greetingLong = 1234;  //Das ist für den Pc eine Zahl
    cout << greetingLong << endl;

    //der Unterschied ist, dass Zahlen für Mathematikaufgaben genutzt werden können
    //werden Zeichenketten aneinandergefügt.
    // 10 + 32
    //Ergebnis bei Zahlen = 42
    //Ergebnis bei Zeichenketten = 1032

    string a = "10";
    string b = "32";

    cout << a+b << endl;

    cout << 10+32 << endl;

    return 0;
}

