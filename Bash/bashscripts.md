# **Bash Scripting**

#### define functions

To define a function you can create a shell script:

for example:

```bash
#!mcd.sh 
mcd () {
    mkdir -p "$1"
    cd "$1"
}
```

This script creates a directory with the name given to the **first Argument** 
and moves into it.

```bash
source mcd.sh
```

defines the function in the shell so that you can call mcd.sh with

```bash
mcd test
```

Arguments are defined with `$1...$9` the number indicates the position of the given argument.

`$?` gives you the error code from the previous command.

```bash
$?  
0  
```
means there was no error in the previous command

`$_` gives you the last argument of the previous command.  

ex:  
```bash
mkdir test
  
cd $_
```    
$_ has the value "test" in that case, so cd will enter the folder that was created in the last command.  

`!!` stores the last command as value.

```bash  
pacman -Syu  
*not enough rigths*  
sudo !!  
password:  
```  
sudo !! in that case is the same as sudo pacman -Syu.

## Logical Operators

#### Or-operator
  
`||`  

bash will execute the commands given in **logical order**, when the first doesnt work it will execute the second.  
If the first command works it will shortcircuit the second command.

```bash
false || echo "Oops Fail!"
```  
will execute the second command

```bash
true || echo "Will not be printed"
```

will not execute the second command
___


#### And-Operator 


`&&` will execute the second command only if the first one works
  
```bash
true && echo "worked well!"
```  

will execute both commands

```bash
false && echo "Will not be printed"
```  

will only execute the first command  
    
### ; Operator

`;` both will always execute

```bash
false ; echo "this will always print"  
this will always print  
true ; echo "told you... always" 
told you... always  
```  


## Variables
#### defining variables

`foo=bar` - sets the value of `foo` to `bar`

`echo $foo` - echoes the value of `foo`


`foo = bar` cannot work because `' '` after `foo` lets the shell think foo is a command
 
Strings can be defined with `"Hello"` or `'Hello'`

`echo "Value is $foo" ` prints: `Value is bar`

while `'Value is $foo'` prints: `Value is $foo`

---


#### storing values in Variables
  
`foo=$(pwd)` stores the output of the `pwd` command into `foo`

```bash
echo "We are in $(pwd)"  
```  
Prints the sentence with the current working directory substituted for `$(pwd)`.


