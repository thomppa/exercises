# Bash Commands

`echo <command>` - prints path to bin of the command

`pwd` - print working directory

`cd` - change working directory

`.` - the current directory
`..` - parent directory
`../../../../../../bin` -  cd to root/bin

`ls` - list files in current directory

`cd /` - cd to root

`cd ~` - cd to /home/user

`cd -` - cd to previous directory (goes both ways)

`mv` - rename the file + move the file or both

`cp` - copy file (rename possible)

`rm` - remove file

`rmdir` - remove empty directory

`mkdir` - make directory (mkdir "Hello World")

`man` - manual pages

`cat` - prints context in file

`tail` - prints the last -n lines

`tee` - writes in file and writes itself in the terminal

`xdg-open` - opens file in appropriate program
## Piping

programs are not made to be compatible to another, but they read the stdin and stdout, so they can be piped together to execute in a chain.

`>` rewire the output of the program into this file
`<` rewire the input of the program into this file

#### Example:

`echo hello > hello.txt`

*the output of echo is stored in hello.txt*

`cat < hello.text` 

*prints hello.txt to terminal*

`cat < hello.txt > hello2.txt`

*copys content of hello.txt in hello2.txt*

`>>` - append to file

`cat < hello.txt >> hello2.txt`

*appends content of hello.txt in hello2.txt*


` | ` - take the output of the program to the left 
and make it to input of the program to the right

#### Example:

`ls -l / | tail -n1`

*prints the last line in the root folder to terminal*

`ls -l / | tail -n1 > ls.text`


*prints the last line in the root folder to the file ls.text*


## User rights

`$` - symbols that you are not running as root
`#` - symbols that the program shall be executed as the root user


