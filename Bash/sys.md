# The /sys

the /sys is not a file system in itself. It merely grants you access to various kernel options like:  


backlight   -- lets you change the brightness of your screen
bdi
block
bluetooth
bsg
dax
devcoredump
devfreq
devfreq-event
dma
dmi
drm
drm_dp_aux_dev
extcon
gpio
graphics
hwmon
i2c-adapter
ieee80211
input
iommu
leds
mei
mem
memstick_host
misc
mmc_host
nd
net
nvme
nvme-subsystem
pci_bus
pci_epc
phy
power_supply
powercap
pps
ptp
pwm
regulator
remoteproc
rfkill
rtc
scsi_device
scsi_generic
scsi_host
sound
spi_master
spi_slave
thermal
tpm
tpmrm
tty
typec
typec_mux
vc
video4linux
vtconsole
wakeup
watchdog
wmi_bus
