# Time-Space Complexity

## Big O

"Big O" is the worst case usage of time and space in relation to the size of the input.


```
Balanced Tree             Big-O

       4                    1
      / \                   |
     2   5                  2
    / \                     |
   1   3                    3
                            |
                            4
                            |
                            5
```


In the well-balanced tree to the right it might take **log(n)** time to find "5"but in worst case it takes ***n*** times to iterate through every single node in the tree.

There aren't that many types of complexity. ***n*** indicates the length of the input and implictely any runtime can be assumed to be multiplied by some constant factor ***k***. **0(n)** is the same as **O(2\*n)**, **O(3\*n)**, **O(4\*n)**, O(***k\*n***)

#### Types of Complexity:

- **Linear. O(n)** - most algorithm run in linear time. Easy to determine if you visit every node or item once and only once. Every Line of code is executed for every node!

- **Constant. 0(*k*)** - Constant time algortithm have a running time independent of the output size. Mathematical formulars for instance have fixed running times and are considered constant time.

- **Logarithmic. O(log(*n*)** - Logarithmic algorithms are often seen in trees. It's best to think of logarithmic as the height of a tree. A binary search often includes traversing down the height of a tree and can be considered logarithmic in time. (In the worst case it's still linear)

- **Superlinear. O(*nlog(n)*)** - most sorts operate in "n log n" time. This includes algorithms like quicksort, mergesort, or heapsort. (quicksort is actually O(n^2) time in the worst-case).

- **Quadratic or Cubic/Polynominal. O(n^2)** - Brute Force algorithms often run in O(n^2) or O(n^3) time where there is a loop within a loop.
  It's eays to spot when there is a for loop inside a for loop, where for each element *i* you iterate through another element *j*, for instance.
  A common scenario is, given two arrays, find the common elements in each array, where you would simply go through each element and and check whether it exists in the other array. This would execute O(n+m) time, where ***n*** and ***m*** are the sizes of each array, It's still great to find these brute force algorithms if you find them.
- **Exponential. O(2^n).** - Exponential algorithms are quite terrible in running time. A classic example is determining every permutation of ***n*** bits (it would take 2^n combinations). Another example is the fibonacci sequence fib(n) = fib(n-1) + fib(n-2), where for each item, it requires the computation of two more subproblems.
- **Factorial. O(n!).** - These algorithms are the slowest and don't show up that often. You might see this in combinatorial problems, or like a "traveling salesman" problem where given ***n*** nodes, you need the optimal path from start to finish. In the first iteration, you have a selection of n-cities to visit, then n-1 cities, then n-2 cities, n-3 cities, etc... until you reach the last city. That runtime is n \* (n-1)n \* (n-2)n \* (n-3)...1 = O(n!).

![Time-Space-Complexity](xZrFEXqLRu9wODp2t5il_bigo.jpg)

Never explain with **O(*m + v + e*)** without defining *mve* first eg. m is the height of the matrix, v are the vertices, e is the number of edges.
  
Time Complexity > Space Complexity.  
Algorithms are a tradeoff between time and space. For example you can take a polynominal algorithm and convert it to an ***0(n)*** algorithm, but it takes a hashmap of of size ***O(n)*** . Good tradeoff.
