#include <stdio.h>

#define SIZE 5
int main(int argc, char** argv)
{
  
  int arr[SIZE];
  arr[0] = 9;
  arr[1] = 27;
  arr[2] = 36;
  arr[3] = 45;
  arr[4] = 5;

  int* ptr = &arr[0];
  //An Array in C Languages IS a Pointer, that points to its first Element, so another way of declaring it would be:
  //int* ptr = arr;

  for (; ; ) {
    printf("%d\n", *ptr);
    if (ptr == &arr[SIZE]-1) {
      break;
	}
    ptr++;
      }

  /* for (int i = 0; i < SIZE; i++) { */
  /*   printf("%d\n", arr[i]); */
  /*     } */
  return 0;
}
