#include <stdio.h>
#include <stdlib.h>

int* create_integer(int default_value)
{
  int* var_ptr = (int*)malloc(sizeof(int));
  *var_ptr = default_value;
  return var_ptr;
}

int main(int argc, char** argv){
  int* ptr = NULL;
  ptr = create_integer(10);
  printf("%d\n", *ptr);
  free(ptr);
  return 0;
}

// Here the created integer variable inside the function is not a local function
// anymore. Instead it is a variable allocated from the heap memory and its
// lifetime is not limited to the function declaring it.

//Eventually the variable becomes deallocated with the free() function as an end to its lifetime

