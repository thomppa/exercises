#include <stdio.h>
#include <stdlib.h>

int *create_an_integer(int default_value) {
  int var = default_value;
  return &var;
}
int main(int argc, char **argv) {
  int* ptr = NULL;
  ptr = create_an_integer(10);
  printf("%d\n", *ptr);
  free(ptr);
  return 0;
}
// if we compile this code we get a segmentation fault error, which usually
// happens, when you point to a local pointer, that is already deallocated.

// the var variable is a local variable and will be deallocated, when we leave
// the function. But its address can be returned from the function.

//Pointing to that adress with ptr in main, we point to an invalid memory address and dereferencing the pointer with the value of that invalid address causes serious problems and the program crashes.

// The Compiler warns about that with "function returns address of local variable"
