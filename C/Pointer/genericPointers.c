#include <stddef.h>
#include <stdio.h>

void printBytes(void* Data, size_t length)
{
  char delim = ' ';
  unsigned char* ptr = Data;

  for (size_t i = 0; i < length; i++) {
    printf("%C 0x%x", delim, *ptr);
    delim = ',';
    ptr++;
      }
  printf("\n");
}
int main(int argc, char** argv)
{
  int a = 9;
  double b = 18.9;

  printBytes(&a, sizeof(int));
  printBytes(&b, sizeof(double));
  return 0;
}
