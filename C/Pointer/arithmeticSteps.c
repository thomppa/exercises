#include <stdio.h>
#include <stdio_ext.h>

int main(int argc, char** argv)
{
  int var = 1;

  int* int_ptr = NULL; // initialize Pointer with nullvalue;
  int_ptr = &var;

  char* char_ptr = NULL;
  char_ptr = (char*) &var;

  printf("Before arithmetic step: int_ptr: %u, char_ptr: %u\n", (unsigned int)int_ptr, (unsigned int)char_ptr);

  int_ptr++;
  char_ptr++;
  
  printf("After arithmetic step: int_ptr: %u, char_ptr: %u\n", (unsigned int)int_ptr, (unsigned int)char_ptr);
  int_ptr++;
  char_ptr++;
  printf("After next arithmetic step: int_ptr: %u, char_ptr: %u\n", (unsigned int)int_ptr, (unsigned int)char_ptr);

  return 0;
}
