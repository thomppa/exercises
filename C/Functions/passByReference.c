#include <stdio.h>

void func(int* a)
{
  int b = 9;
  *a = 5;
  a = &b;
}

int main(int argc, char** argv)
{
  int x = 3;
  int* xptr = &x;
  printf("Before Function Call: %d\n", x);
  printf("Pointer before function Call: %p\n", (void*)xptr);
  func(xptr);
  printf("After Function Call: %d\n", x);
  printf("Pointer after function Call: %p\n", (void*)xptr);
  return 0;
}

// As we can See, the value of the Pointer is not changed by the function call,
// which means, that the pointer is passed as a pass-by-value argument.
// Dereferencing the Pointer in the function has allowed accessing the variable
// where the pointer is pointing to.


// But you can see that changing the value of the pointerParameter in the
// function did not change it's counterpart argument int the caller function;

// During a function call, all arguments are passed by value and dereferencing
// the pointers allows the modification of the caller functions variables

