#include <stdio.h>

// To access this Structure we have to write struct sample_t <instanceName>
/* struct sample_t */
/* { */
/*   char first; */
/*   char second; */
/*   char third; */
/*   short fourth; */
/* }; */

// To avoid this, we use a typedef and name the Struct after the definition
// The Memory Size of this struct is 6 Bytes instead of 5 Bytes.
// The Reason for this is, that the cpu 
/* typedef struct{ */
/*   char first; */
/*   char second; */
/*   char third; */
/*   short fourth; */
/* } sample_t; */

typedef struct __attribute__((__packed__))
{
  char first;
  char second;
  char third;
  short fourth;  
} sample_t;

void print_size(sample_t* var){
  printf("Size %lu bytes\n", sizeof(*var));
}

void print_bytes(sample_t* var){
  unsigned char* ptr = (unsigned char*) var;
  for (int i = 0; i < sizeof(*var); i++, ptr++) {
    printf("%d ", (unsigned int)*ptr);
      }
  printf("\n");
}

int main(int argc, char** argv)
{
  sample_t var;
  var.first = 'A';
  var.second = 'B';
  var.third = 'C';
  var.fourth = 765;
  print_size(&var);
  print_bytes(&var);
  printf("%lu \n",sizeof(short));
}
