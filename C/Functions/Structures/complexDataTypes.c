#include <stdio.h>

// This struct is a simple UDT (User Defined Type) because it is only made out
// of (PDT's / Primitive Data Types)
typedef struct{
  int x;
  int y;
} point_t;

// The next two structs contain a UDT, which makes them complex UDT's
typedef struct{
  point_t center;
  int radius;
} circle_t;

typedef struct{
  point_t start;
  point_t end;
} line_t;

int main(int argc, char** argv)
{
  circle_t c;
  circle_t* p1 = &c;
  point_t* p2 = (point_t*)&c;
  int* p3 = (int*)&c;

  printf("p1: %p \n", (void*)p1);
  printf("p2: %p \n", (void*)p2);
  printf("p3: %p \n", (void*)p3);
  printf("Size of Point Type: %lu\n", sizeof(point_t));
  printf("Size of Circle Type: %lu\n", sizeof(circle_t));
  printf("Size of Line Type: %lu\n", sizeof(line_t));
  
}
