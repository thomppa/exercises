#include <stdio.h>


void func(int a) { a = 5; }

int main(int argc, char** argv)
{
  int x = 3;
  printf("Before Function call: %d\n", x);
  func(x);
  printf("Before Function call: %d\n", x);
  return 0;
}

//Nothing changes about the x value here, because it is passed by value.
