#include <stdio.h>

#define TRUE 1
#define FALSE 0

int sum(int a, int b){ return a + b; }

int subtract(int a, int b) { return a - b; }

_Bool isSum;

int main(){
  int(*func_ptr)(int, int);
  func_ptr = NULL;
  isSum = FALSE;

  if (isSum)
    {
      func_ptr = &sum;
    }
  else
    {
      func_ptr = &subtract;
    }

  int result = func_ptr(4,5);
  printf("Result is: %d\n", result);
  
}
