#include <stdio.h>
#include <string.h>

#define LOOP(v, s, e) for (v = s; v <= e; v++) {
#define PRINT(printTarget) printf("%d\n", printTarget);
#define ENDLOOP }

int main()
{

  int counter = 0;

  LOOP(counter, 1, 10)
    PRINT(counter)
    ENDLOOP
  
  return 0;
}
