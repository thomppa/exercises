# Compilation

- [Compilation](#compilation)
	- [Rules of compilation](#rules-of-compilation)
	- [Step 1 Preprocesing](#step-1-preprocesing)
	- [Step 2 Compilation](#step-2-compilation)
	- [Step 3 Assembly](#step-3-assembly)
	- [Step 4 Linking](#step-4-linking)
- [Preprocessing](#preprocessing)
- [Compiler](#compiler)
- [Abstract Syntax Tree (AST)](#abstract-syntax-tree-ast)
- [Assembler](#assembler)
- [Linker](#linker)


## Rules of compilation

there are 2 important rules for compilation:

- We only compile source files
- We compile each file separately

## Step 1 Preprocesing
Given the following script we want to output a piece of assembly code for it.

```C
#include "average.h"

double avg(int * array, int length, average_type_t type){
  if (length <= 0 || type == NONE ) {
    return 0;
      }
  double sum = 0.0;
  for (int i = 0; i < length; i++) {
    if (type == NORMAL) {
      sum += array[i];
	}
    else if (type == SQUARED) {
      sum += array[i] * array[i];
	}
      }
  return sum;
}

```

The first step is to gather all informations from the header files in a single body of C code.  

Also all other preprocessing directives have to be resolved in this step.

This preprocessed piece of code is called a translation unit, which is then ready to be compiled.

Sometimes a translation unit is also called a compilation unit.
```bash
$ gcc -E <filename.c>
```

outputs a translation Unit which can be read through dumping it in a file.

```bash
$ gcc -E <filename.c> > dump.c
```

dump c then contain following code:

```C
# 0 "average.c"
# 0 "<built-in>"
# 0 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 0 "<command-line>" 2
# 1 "average.c"
# 1 "average.h" 1


typedef enum { NONE, NORMAL, SQUARED } average_type_t;

double avg(int *, int, average_type_t);
# 2 "average.c" 2

double avg(int * array, int length, average_type_t type){
  if (length <= 0 || type == NONE ) {
    return 0;
      }
  double sum = 0.0;
  for (int i = 0; i < length; i++) {
    if (type == NORMAL) {
      sum += array[i];
 }
    else if (type == SQUARED) {
      sum += array[i] * array[i];
 }
      }
  return sum;
}
```

The translation unit 

## Step 2 Compilation

Once we have the translation unit we are ready for the 2nd step, which is the compilation. The input for this step is the output of the Preprocessing step and the ot put will be the corresponding ***assembly code***

The assembly code is still human readable, but is machine dependend and close to the hardware. It needs further processing in order to become machine-level instructions.

We can always stop the second step and dump the resulting assembly code in a file with the `-S` option. The output is a file with the corresponding name and a `.s` ending.

```bash
$ gcc -S <filename-c>
```
outputs:

```C
	.file	"average.c"
	.text
	.globl	average
	.type	average, @function
average:
.LFB0:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	movq	%rdi, -24(%rbp)
	movl	%esi, -28(%rbp)
	movl	%edx, -32(%rbp)
	cmpl	$0, -28(%rbp)
	jle	.L2
	cmpl	$0, -32(%rbp)
	jne	.L3
.L2:
	movl	$0, %eax
	jmp	.L4
.L3:
	pxor	%xmm0, %xmm0
	movsd	%xmm0, -8(%rbp)
	movl	$0, -12(%rbp)
	jmp	.L5
.L8:
	cmpl	$1, -32(%rbp)
	jne	.L6
	movl	-12(%rbp), %eax
	cltq
	leaq	0(,%rax,4), %rdx
	movq	-24(%rbp), %rax
	addq	%rdx, %rax
	movl	(%rax), %eax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	-8(%rbp), %xmm1
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -8(%rbp)
	jmp	.L7
.L6:
	cmpl	$2, -32(%rbp)
	jne	.L7
	movl	-12(%rbp), %eax
	cltq
	leaq	0(,%rax,4), %rdx
	movq	-24(%rbp), %rax
	addq	%rdx, %rax
	movl	(%rax), %edx
	movl	-12(%rbp), %eax
	cltq
	leaq	0(,%rax,4), %rcx
	movq	-24(%rbp), %rax
	addq	%rcx, %rax
	movl	(%rax), %eax
	imull	%edx, %eax
	pxor	%xmm0, %xmm0
	cvtsi2sdl	%eax, %xmm0
	movsd	-8(%rbp), %xmm1
	addsd	%xmm1, %xmm0
	movsd	%xmm0, -8(%rbp)
.L7:
	addl	$1, -12(%rbp)
.L5:
	movl	-12(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jl	.L8
	movsd	-8(%rbp), %xmm0
	cvttsd2sil	%xmm0, %eax
.L4:
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE0:
	.size	average, .-average
	.ident	"GCC: (GNU) 11.1.0"
	.section	.note.GNU-stack,"",@progbits

```
## Step 3 Assembly

The next step after compilation is **assembly**. The objective will be to generate an object file with actual machine-level instructions.
Each architecture will have its own assembler with its own assembly code to its own machine code.

To pull both of the previous steps together out goal here is to create a ***relocatable object file***.  
Every other product we create will be based on the relocatable object file.

	- Binary file and object file are synonyms 
	  that refer to files containing machine-level instructions

In most UNIX like OS we have an assembler tool called `as` which can be used to create a relocatable object file from an assembly file.

```bash
$as average.s -o average.o
```

Despite the fact, that the assembler can be used directly. It is not recommended.  
Good Practice would be to use the compiler to call `as` indirectly to generate the relocatable object file.

If we pass the `-c` option to almost all known C-Compilers it will generate the corresponding object file. And is equivalent f performing all 3 previous Steps together.

```bash
$gcc -c average.c
```

All of the steps that we have just done - preprocessing, compilation, assembling - are done in just this single command. The output will be a relocatable object file with the same name as the input file, but with a `.o` extension.

If we have several source files to compile we just run the command for all source files, like this:

```bash
$gcc -c average.c -o avg.o
$gcc -c main.c -o main.o
```

with the `-o` option we can specify a name for the output file.

## Step 4 Linking

The last step is to create an executable file from all relocatable object files. This step is known as linking.

the command for linking or object files would be:

```bash
$ld avg.o main.o
```

but this would result in errors.
```bash
$ld avg.o main.o
ld: warning: cannot find entry symbol _start; defaulting to 0000000000401000
ld: main.o: in function `main':
main.c:(.text+0x80): undefined reference to `printf'
ld: main.c:(.text+0xbc): undefined reference to `printf'
ld: main.c:(.text+0xd5): undefined reference to `__stack_chk_fail'
```

Those errors are generated, because the object files, that contain the definition of the `printf` function and the `__stack_chk_fail` are not generated with the last command.

It is complicated to use the linker directly because w would need to compile much more object files to get a suitable result.

Therefore it is easier to use the compiler directly to communicate with the linker and pass the required objects to it.

```bash
$gcc avg.o main.o
```

will finally result in an executable file.

to specify the name of the executable we can again add the `-o` option to the command as in:

```bash
$gcc -o <executable name> avg.o main.o
```

# Preprocessing

The Proprocessor allows us to modify our source code before sending it for compilation.  
At the same time it allows us to divide our source code, especially the declarations into header/- files, to be reused as an include in multiple source files.

It is unique to C-like languages.

It is vital to remember, that the Preprocessor only does some simple text substitution tasks.  

 **It does not know anything about the C Syntax**.

 Example:

```C
#define file 1000

Hello this is just a simple text file but ending with a .c extension!

This is not a real C file for sure!

But we can Preprocess it!
```
The Preprocessed code would be looking like this.

```C
# 0 "sample.c"
# 0 "<built-in>"
# 0 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 0 "<command-line>" 2
# 1 "sample.c"


Hello this is just a simple text 1000 but ending with a .c extension!

This is not a real C 1000 for sure!

But we can Preprocess it!
```
As we can see all occurences of *file* have been substituted with *1000*. Because defined it like that in the header file. The definition itself is not part of the output.

The Preprocessor uses a parser, which looks for directives in the input code.

In Most Unix like systems there is a tool called `cpp`.  
Which stands for C Pre-Processor and not C Plus Plus.  
Instead of using `gcc` we can then communicate with the Preprocessor directly with the command:

```bash
$cpp average.c
```
Again outputs:
```C
# 0 "average.c"
# 0 "<built-in>"
# 0 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 0 "<command-line>" 2
# 1 "average.c"
# 1 "average.h" 1


typedef enum { NONE, NORMAL, SQUARED } average_type_t;

double avg(int *, int, average_type_t);
# 2 "average.c" 2

double avg(int * array, int length, average_type_t type){
  if (length <= 0 || type == NONE ) {
    return 0;
      }
  double sum = 0.0;
  for (int i = 0; i < length; i++) {
    if (type == NORMAL) {
      sum += array[i];
 }
    else if (type == SQUARED) {
      sum += array[i] * array[i];
 }
      }
  return sum;
}
```

As a final Note: If we pass a file with the .i extension to a C Preprocessor, then it will bypass the preprocessor step. A file with the `.i` extension is supposed to already be preprocessed. Therefore it should be sent directly to the compilation step.

# Compiler


# Abstract Syntax Tree (AST)


As Explained in the section [Compiler](#compiler) a C Compiler should parse the translation unit and create an intermediate data structure.  
The compiler creates this intermediate data structure according to the C Grammar and saves the result in a tree like data structure, **which is not architecture dependent**.  

```C
int main(int argc, char** argv)
{
  int var1 = 1;
  double var2 = 2.5;
  int var3 = var1 + var2;
  return 0;
}
```
```bash
$clang -Xclang -ast-dump -fsyntax-only abstractSyntaxTreeSource.c
```

The final data structure is commonly reffered to as an Abstract Syntax Tree (AST).

AST's can be generated for any programming language, so the AST structure must be abstract enough to be independent of C Syntax.

This is why we can find the GCC or LLVM as compilers for many languages beyond C and C++.  

Once the AST is produced, the compiler can start optimize the AST for the target architecture.

```C
TranslationUnitDecl 0x558587706f78 <<invalid sloc>> <invalid sloc>
|-TypedefDecl 0x558587707830 <<invalid sloc>> <invalid sloc> implicit __int128_t '__int128'
| `-BuiltinType 0x558587707510 '__int128'
|-TypedefDecl 0x5585877078a0 <<invalid sloc>> <invalid sloc> implicit __uint128_t 'unsigned __int128'
| `-BuiltinType 0x558587707530 'unsigned __int128'
|-TypedefDecl 0x558587707ba8 <<invalid sloc>> <invalid sloc> implicit __NSConstantString 'struct __NSConstantString_tag'
| `-RecordType 0x558587707980 'struct __NSConstantString_tag'
|   `-Record 0x5585877078f8 '__NSConstantString_tag'
|-TypedefDecl 0x558587707c40 <<invalid sloc>> <invalid sloc> implicit __builtin_ms_va_list 'char *'
| `-PointerType 0x558587707c00 'char *'
|   `-BuiltinType 0x558587707010 'char'
|-TypedefDecl 0x558587747ed0 <<invalid sloc>> <invalid sloc> implicit __builtin_va_list 'struct __va_list_tag [1]'
| `-ConstantArrayType 0x558587707ee0 'struct __va_list_tag [1]' 1
|   `-RecordType 0x558587707d20 'struct __va_list_tag'
|     `-Record 0x558587707c98 '__va_list_tag'
`-FunctionDecl 0x5585877480d0 <abstractSyntaxTreeSource.c:1:1, line:7:1> line:1:5 main 'int (int, char **)'
  |-ParmVarDecl 0x558587747f40 <col:10, col:14> col:14 argc 'int'
  |-ParmVarDecl 0x558587747ff0 <col:20, col:27> col:27 argv 'char **'
  `-CompoundStmt 0x5585877484c0 <line:2:1, line:7:1>
    |-DeclStmt 0x558587748268 <line:3:3, col:15>
    | `-VarDecl 0x5585877481e0 <col:3, col:14> col:7 used var1 'int' cinit
    |   `-IntegerLiteral 0x558587748248 <col:14> 'int' 1
    |-DeclStmt 0x558587748320 <line:4:3, col:20>
    | `-VarDecl 0x558587748298 <col:3, col:17> col:10 used var2 'double' cinit
    |   `-FloatingLiteral 0x558587748300 <col:17> 'double' 2.500000e+00
    |-DeclStmt 0x558587748478 <line:5:3, col:25>
    | `-VarDecl 0x558587748350 <col:3, col:21> col:7 var3 'int' cinit
    |   `-ImplicitCastExpr 0x558587748460 <col:14, col:21> 'int' <FloatingToIntegral>
    |     `-BinaryOperator 0x558587748440 <col:14, col:21> 'double' '+'
    |       |-ImplicitCastExpr 0x558587748428 <col:14> 'double' <IntegralToFloating>
    |       | `-ImplicitCastExpr 0x5585877483f8 <col:14> 'int' <LValueToRValue>
    |       |   `-DeclRefExpr 0x5585877483b8 <col:14> 'int' lvalue Var 0x5585877481e0 'var1' 'int'
    |       `-ImplicitCastExpr 0x558587748410 <col:21> 'double' <LValueToRValue>
    |         `-DeclRefExpr 0x5585877483d8 <col:21> 'double' lvalue Var 0x558587748298 'var2' 'double'
    `-ReturnStmt 0x5585877484b0 <line:6:3, col:10>
      `-IntegerLiteral 0x558587748490 <col:10> 'int' 0
```

Each compiler has its own AST implementation so there is no need to analyze this example any further now.

But if we take a closer look to the figure above we can find a line that starts with `-FunctionDecl`  
This represents the main function and the before that we find meta information regarding the translation unit passed to the computer.

If we continue after `FunctionDecl` we will find tree entries -or nodes- for declaration statements, binary operation statements, the return statement and even implicit cast statements.

Another benefit of having an AST for source code is that you can rearrange the order of instructions, prune some unused branches, replace branches, so that we hae better performance in a program but preserve the general purpose of a program.

# Assembler

A platform has to have an assembler in order to produce object files that conatin machine level instructions.  
In a Unix like system the assembler is invoked with the `as` command.

If we install two different Unix like systems on the same architecture, the installed assemblers might not be the same.  

That means, despite the fact, that the machine level instructions are the same, the object files are different.  
In other words, each operating system defines it's own specific binary format or *object file format*, when it comes to storing machine level instructions.  

Therefore 2 Factors specify the contents of an object file:
- The architecture
- the operating system

		-typically the term 'platform' is used for that combination

To end this section we say that object files, hence the assembler, are platform specific.

In Linux we use the **Executable and Linking Format (ELF).**

As the name implies, all executable files, object files and shared libraries should use this format.

# Linker



