# 0 "average.c"
# 0 "<built-in>"
# 0 "<command-line>"
# 1 "/usr/include/stdc-predef.h" 1 3 4
# 0 "<command-line>" 2
# 1 "average.c"
# 1 "average.h" 1


typedef enum { NONE, NORMAL, SQUARED } average_type_t;

double avg(int *, int, average_type_t);
# 2 "average.c" 2

double avg(int * array, int length, average_type_t type){
  if (length <= 0 || type == NONE ) {
    return 0;
      }
  double sum = 0.0;
  for (int i = 0; i < length; i++) {
    if (type == NORMAL) {
      sum += array[i];
 }
    else if (type == SQUARED) {
      sum += array[i] * array[i];
 }
      }
  return sum;
}
