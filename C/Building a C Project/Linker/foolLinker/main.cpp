#include <cstdlib>
#include <stdlib.h>
#include <stdio.h>
#include "declarations.h"

int main(int argc, char** argv)
{
  Addition* adder = new Addition();
  int arr[] = {3,5,4,3,2};
  int x = adder -> add(5,6);
  int y = adder -> add(arr);
  free(adder);
  printf("Result x: %d\n",x);
  printf("Result y: %d\n",y);
}
