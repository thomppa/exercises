#include <stdio.h>
#include "average.h"

int main(int argc, char** argv)
{
  int array[5] = {10,3,5,-8,9};
  int length = sizeof(array) / sizeof(array[0]);

  double average = avg(array, length, NORMAL);
  printf("The normal average: %f\n", average);

  average = avg(array, length, SQUARED);
  printf("The squared average is: %f\n", average);

  return 0;

}
