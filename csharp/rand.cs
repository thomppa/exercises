using System;
using System.Resources;

namespace RandomPi
{
    class Program
    {
        static double GenerateDigit(Random rng)
        {
            return (double)rng.NextDouble();
        }

        static double Estimate_Pi(long n)
        {
            
            double numPointsCircle = 0;
            double numPointsTotal = 0;
            
            for (int i = 0; i < n; i++)
            {
                Random rng = new Random();
                GenerateDigit(rng);
                double x = rng.NextDouble();
                double y = rng.NextDouble();

                //Console.WriteLine(x);
                //Console.WriteLine(y);

                double distance = (x * x) + (y * y); 
                
                numPointsTotal += 1;
                
                if (distance <= 1)
                {
                    numPointsCircle += 1;
                }
            }

            Console.WriteLine(numPointsCircle);
            Console.WriteLine(numPointsTotal);
            return 4 * numPointsCircle / numPointsTotal;
        }
        
        static void Main(string[] args)
        {
            Console.WriteLine(Estimate_Pi(10000000));
        }
    }
}
