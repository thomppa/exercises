using System;

public class RecursionOne
{

    public static void Main()
    {
	RecursionOne rc = new RecursionOne();
	RecursionTwo r2c = new RecursionTwo();
	
	Console.WriteLine(rc.sum(5));

	Console.WriteLine("The Maximum number of Unique ways from top left to bottom right in a 4x4 grid is");
	Console.WriteLine(r2c.GridPaths(5,5));
    }

    private int sum(int n)
    {
	if(n == 0)
	    return 0;
	else
	    return n + sum(n-1);
    }
}

public class RecursionTwo
{
    public int GridPaths(int rows, int columns)
    {
	if(rows == 1 || columns == 1)
	    return 1;
	else
	    return GridPaths(rows, columns -1) + GridPaths(rows-1 , columns);
    }
}

