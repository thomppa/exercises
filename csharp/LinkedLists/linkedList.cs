using System;

public class LinkedList
{
    Node head; //Head of the List


    public class Node
    {
        public int data;
        public Node next;
        public Node(int d)
        {
            data = d;
            next = null;
        } //Constructor for the 
    }

    public void printList()
    {
        Node n = head;
        while(n != null)
        {
            Console.WriteLine(n.data + " ");
            n = n.next;
        }
    }

    public static void Main()
    {
        LinkedList llist = new LinkedList();

        llist.head = new Node(1);
        Node second = new Node(2);
        Node third = new Node(3);

        llist.head.next = second;
        second.next = third;

        llist.printList();
    }

}
