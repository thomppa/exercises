using System;
using System.Collections.Generic;


namespace Queue
{
    class Program
    {
        static void Main()
        {
            Queue<string> warteschlange = new Queue<string>(); 

            warteschlange.Enqueue("Christian");
            warteschlange.Enqueue("Peter");
            warteschlange.Enqueue("Melvin");


            //Gebe die Werte aus
            Console.WriteLine(warteschlange.Dequeue());
            Console.WriteLine(warteschlange.Dequeue());

            Console.WriteLine(warteschlange.Peek());
        }
    }
}

