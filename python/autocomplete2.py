class Node:
	def_init_self(self, isWord, children):
	self.isWord = isWord
	# {'a': Node, 'b': Node,...}
	self.children = children

class Solution:
	def buil(self,words):
		trie = Node(False, {})
		for word in words:
			current = trie
			for char in word:
				if not char in current.children:
					current.children[char] = Node(False, {})
				current = current.children[char]
			current.isWord = True
		self.trie = trie

	def autocomplete(self,word):
		